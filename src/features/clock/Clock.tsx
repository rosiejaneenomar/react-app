import React from 'react';
import { FormatDate } from './FormatDate';

export class Clock extends React.Component<any, any> {
	timerID: any;

	constructor(props: any) {
		super(props);
    console.log(props.lastName);
		this.state = {
			date: new Date(),
			name: `${props.name} ${props.lastName}`,
		};
	}


  FormattedDateFn(props: {date: Date}) {
    return <h2>FormattedDateFn - It is {props.date.toLocaleTimeString()}.</h2>;
  }

  
	componentDidMount() {
    console.log('componentDidMount');
		this.timerID = setInterval(() => this.tick(), 1000);
	}

	componentWillUnmount() {
    console.log('componentWillUnmount');
		clearInterval(this.timerID);
	}

	tick() {
		this.setState({
			date: new Date(),
		});
	}
	render() {
		return (
			<div>
				<h1>Hello, world!</h1>
				<this.FormattedDateFn date={this.state.date}></this.FormattedDateFn>
        <FormatDate date={this.state.date}></FormatDate>
				
			</div>
		);
	}
}
