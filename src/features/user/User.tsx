
import React from 'react';
import styles from './User.module.css';

export class User extends React.Component<any, any> {

  constructor(props: any) {
    super(props)
    console.log('constructor - props', props);
    console.log('constructor - this.props', this.props);

    this.state = {
      name: null
    };

  }

  onClickName(value: string) {
    console.log('onClickName');
    alert(`my name is ${this.state.name} - ${value}`);
  }

  //ARROW FUNCTION
  onChangeName2 = (e: any) => {
    console.log('onChangeName2', e);
    this.setState({
        count: this.state.name
    });
}

  onChangeName(e: any) {
    console.log('onChangeName', e);
    this.setState({name: e.target.value});
  }


  render() {
    return (
      <div>
        <input 
          type="text" 
          placeholder="Input name" 
          value={this.state.name == null ? '' : this.state.name}
          onChange={this.onChangeName.bind(this)}/> 
        <h2>Props - My name is {this.props.name}.</h2>
        <h2>State - My name is {this.state.name}.</h2>
        <button 
          aria-label="Click me"
          className={styles.primaryBtn}
          onClick={this.onClickName.bind(this, 'test')}>
            Click me
          </button>
      </div>
    )
  }

}